#!/bin/bash -x

# Query info from the display
function BI_status()
{
  curl -s http://busyindicator/api/display | jq .
}

# Send a text string to the display
# >>> BI_display "HELLO WORLD" 4
function BI_display()
{
  curl --header "Content-Type: application/json" \
       --data "{\"text\": \"$1\", \"size\": ${2:-4}, \"delay\": 500}" \
       http://busyindicator/api/display
}

# Send a graphics file to the display
# Can use http://javl.github.io/image2cpp/ to convert to code array, then compile the array to binary.
# (There's probably a much easier way...)
# Up to 4 frames (128x32, bitvalue 0=black) concatenated into one file.
# >>> BI_bitmap bongocat.bin
function BI_bitmap()
{
  curl -F "data=@$1" http://busyindicator/api/bitmap
}

# Select a graphics file from SPIFFS
# (Put the file in /data/bitmaps and upload the filesystem image.)
# >>> BI_bitmap bongocat.bin
function BI_filename()
{
  curl --header "Content-Type: application/json" \
       --data "{\"file\": \"$1\", \"delay\": ${2:-100}}" \
       http://busyindicator/api/bitmap
}

# Save some typing... (tab complete FTW)
function BI_bongocat()
{
  BI_filename "bongocat.bin"
}
