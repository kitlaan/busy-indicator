#include <Arduino.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>

#include <ArduinoOTA.h>

#define BUTTON_A  0
#define BUTTON_B 16
#define BUTTON_C  2

// Settings defined using platformio_local.ini
#if !defined(HOSTNAME) || !defined(WIFISSID) || !defined(WIFIPASS)
  #error Missing settings from build configuration
#endif

AsyncWebServer server(80);

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);

// 1: 21-char, 4-row
// 2: 10-char, 2-row
// 3:  7-char
// 4:  5-char
const int MSGSIZE_OFFSETS[] = {0, 0, 4, 0};
const int MSGSIZE_MAXLEN[] = {21, 10, 7, 5};

bool msg_valid = false;
int  msg_size = 4;
char msg_text[128] = {0};
int  msg_pos = 0;
int  msg_len = 0;
int  msg_delay = 200;

bool    bitmap_valid = false;
int     bitmap_frames = 0;
uint8_t bitmap_data[4*512];
int     bitmap_pos = 0;
int     bitmap_delay = 100;

uint8_t staging_data[4*512];

bool display_update = false;
uint32_t refresh_last = 0;
uint32_t refresh_delay = 0;

void setup()
{
  pinMode(BUTTON_A, INPUT_PULLUP);
  pinMode(BUTTON_B, INPUT_PULLUP);
  pinMode(BUTTON_C, INPUT_PULLUP);

  Serial.begin(9600);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32

  // Display pre-initialized Adafruit splashscreen
  display.display();
  delay(1000);

  // Clear the buffer and display initialization
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0,0);
  display.println("Connecting to:");
  display.println(WIFISSID);
  display.println();
  display.print(__DATE__ " " __TIME__);
  display.display(); // actually display all of the above

  // Initialize SPIFFS
  SPIFFS.begin();

  // Prepare wifi
  WiFi.persistent(false);
  WiFi.hostname(HOSTNAME);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFISSID, WIFIPASS);

  // Spin here until wifi is connected
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }

  // Clear display as we're connected
  display.clearDisplay();
  display.display();

  // Prepare for network visibility
  MDNS.begin(HOSTNAME);
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("busyindicator", "tcp", 80);

  // Prepare OTA support
  ArduinoOTA.setHostname(HOSTNAME);
  ArduinoOTA.begin();

  // ENDPOINT: static files
  server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

  // ENDPOINT: GET display
  // curl http://hostname/display
  server.on("/api/display", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncResponseStream  *response = request->beginResponseStream("application/json");
    DynamicJsonDocument root(1024);

    root["text"] = msg_valid ? msg_text : bitmap_valid ? "<GFX>" : "";
    root["size"] = msg_valid ? msg_size : 4;
    root["delay"] = msg_valid ? msg_delay : bitmap_valid ? bitmap_delay : 200;

    serializeJson(root, *response);
    request->send(response);
  });

  // ENDPOINT: POST display
  // curl --header "Content-Type: application/json" --data '{"text": "Hi", "size": 2}' http://hostname/display
  server.addHandler(new AsyncCallbackJsonWebHandler(
        "/api/display", [](AsyncWebServerRequest *request, JsonVariant &json) {
    int code = 400;

    if (json.isNull()) {
      code = 200;
    }
    else {
      JsonObjectConst jsonObj = json.as<JsonObject>();
      if (!jsonObj.isNull()) {
        msg_delay = max(50, min(jsonObj["delay"] | 200, 1000));
        msg_size = max(1, min(jsonObj["size"] | 1, 4));
        strlcpy(msg_text, jsonObj["text"] | "", sizeof(msg_text));

        msg_len = strlen(msg_text);
        msg_pos = 0;

        msg_valid = true;
        bitmap_valid = false;

        display_update = true;

        code = 200;
      }
    }

    request->send(code);
  }));

  // ENDPOINT: POST bitmap
  // curl --header "Content-Type: application/json" --data '{"file": "bongocat.bin", "delay": 50}' http://hostname/bitmap
  server.addHandler(new AsyncCallbackJsonWebHandler(
        "/api/bitmap", [](AsyncWebServerRequest *request, JsonVariant &json) {
    int code = 400;

    if (json.isNull()) {
      code = 200;
    }
    else {
      JsonObjectConst jsonObj = json.as<JsonObject>();
      if (!jsonObj.isNull()) {
        bitmap_delay = max(50, min(jsonObj["delay"] | 100, 1000));

        const char* filename = jsonObj["file"];
        if (filename && filename[0]) {
          char buffer[128];
          snprintf(buffer, sizeof(buffer), "/bitmaps/%s", filename);
          File fp = SPIFFS.open(buffer, "r");
          if (fp) {
            size_t len = fp.readBytes((char*)staging_data, sizeof(staging_data));
            fp.close();

            bitmap_frames = min(len / 512, (size_t)4);
            memcpy(bitmap_data, staging_data, len);

            bitmap_pos = 0;

            bitmap_valid = bitmap_frames > 0;
            msg_valid = false;

            display_update = true;

            code = 200;
          }
          else {
            code = 415;
          }
        }
      }
    }

    request->send(code);
  }));

  // ENDPOINT: PUT bitmap
  // up to 4 files (512 bytes of packed 128x32, concatenated)
  // http://javl.github.io/image2cpp/
  server.on("/api/bitmap", HTTP_POST|HTTP_PUT, [](AsyncWebServerRequest *request) {
    request->send(bitmap_frames > 0 ? 200 : 400);
  }, [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) {
    if (index == 0) {
      memset(staging_data, 0, sizeof(staging_data));
    }
    if (index < sizeof(staging_data)) {
      size_t maxcopy = min(len, sizeof(staging_data) - index);
      memcpy(staging_data + index, data, maxcopy);
    }
    if (final) {
      bitmap_frames = min((index + len) / 512, (size_t)4);
      memcpy(bitmap_data, staging_data, sizeof(bitmap_data));

      bitmap_pos = 0;

      bitmap_valid = bitmap_frames > 0;
      msg_valid = false;

      display_update = true;
    }
  });

  // Start web server
  server.begin();
}

void loop()
{
  ArduinoOTA.handle();

  if (Update.isRunning()) {
    return;
  }

  // don't refresh the display constantly if we dont need to
  if (display_update || (refresh_delay > 0 && (millis() - refresh_last) > refresh_delay)) {
    refresh_last = millis();
    display_update = false;

    if (msg_valid) {
      bool scrolling = (msg_size == 3 && msg_len > 7) || (msg_size == 4 && msg_len > 5);

      refresh_delay = scrolling ? msg_delay : 0;

      display.clearDisplay();
      display.setTextSize(msg_size);
      display.setTextColor(SSD1306_WHITE);
      display.setCursor(0, MSGSIZE_OFFSETS[msg_size-1]);

      if (!scrolling) {
        display.print(msg_text);
      }
      else {
        char output[8];
        if (msg_pos < msg_len) {
          strlcpy(output, msg_text + msg_pos, MSGSIZE_MAXLEN[msg_size-1] + 1);
        }
        else {
          memset(output, ' ', sizeof(output));
        }
        if (msg_len - msg_pos < MSGSIZE_MAXLEN[msg_size-1] - 3) {
          if (msg_len - msg_pos + 0 >= 0) {
            output[msg_len - msg_pos + 0] = ' ';
          }
          if (msg_len - msg_pos + 1 >= 0) {
            output[msg_len - msg_pos + 1] = ' ';
          }
          if (msg_len - msg_pos + 2 >= 0) {
            output[msg_len - msg_pos + 2] = ' ';
          }
          strlcpy(output + ((msg_len - msg_pos) + 3), msg_text, sizeof(output) - ((msg_len - msg_pos) + 3));
        }

        display.print(output);

        msg_pos = (msg_pos + 1) % (msg_len + 3);
      }
    }
    else if (bitmap_valid) {
      refresh_delay = bitmap_frames > 1 ? bitmap_delay : 0;

      display.clearDisplay();
      // bit=1 means draw white
      display.drawBitmap(0, 0, (uint8_t*)bitmap_data + (512 * bitmap_pos), 128, 32, SSD1306_WHITE);

      bitmap_pos = (bitmap_pos + 1) % bitmap_frames;
    }

    display.display();
  }

#if 0
  if (!digitalRead(BUTTON_A)) display.print("A");
  if (!digitalRead(BUTTON_B)) display.print("B");
  if (!digitalRead(BUTTON_C)) display.print("C");
#endif

  delay(10);
  yield();
}